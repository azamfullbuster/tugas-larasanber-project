<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->get();

        $userList = UserResource::collection($users);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User Data List',
            'users' => $userList,
        ]);
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
        ]);

        $newUser = new UserResource($user);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User Data Added!',
            'data' => $newUser,
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->update();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User data edited!',
            'data' => $user,
        ]);
    }

    public function delete($id)
    {
        User::destroy($id);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'User Has been deleted!',
        ]);
    }
}
