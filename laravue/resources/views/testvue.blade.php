<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <div id="app">
        <input v-model="nama" id="userName"></input>
        <button v-if="editStat === 0" v-on:click="add">Add</button>
        <button v-if="editStat === 1" v-on:click="update">Update</button>


        <ul>
            <li v-for="(user, index) in users">
                @{{user.name}} ||
                <button v-on:click="edit(index, user)">Edit</button>
                <button v-on:click="remove(index, user)">Delete</button>
            </li>
        </ul>

    </div>

    <script src="https://cdn.jsdelivr.net/npm/vue@2"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script>
        var vue = new Vue({
            el: '#app',
            data: {
                users: [],
                editStat: 0,
                nama: '',
                arr: 0,
                temp_id: 0,
            },
            methods: {
                add: function () {
                    nameValue = document.getElementById('userName').value;
                    
                    axios.post(`api/users`, {name: nameValue})
                    .then(response => {
                    this.users.unshift({name: nameValue});
                    vue.nama = '';
                    console.log(vue.users);
                    })
                },
                edit: function (index, user) {
                    vue.editStat = 1;
                    vue.arr = index;
                    vue.temp_id = user.id;
                    vue.nama = vue.users[index].name;
                    console.log(vue.users);
                },
                update: function () {
                    nameValue = document.getElementById('userName').value;
                    
                    axios.put(`api/users/update/` + vue.temp_id, {name: nameValue})
                    .then(response => {
                    vue.users[vue.arr].name = nameValue;
                    vue.nama = '';
                    vue.editStat = 0;
                    console.log(response.data);
                    })
                    
                },
                remove: function (index, user) {
                    if (confirm("Do you really want to delete this user ?")) {
                        
                        axios.delete(`api/users/delete/` + user.id)
                        .then(response => {
                        vue.users.splice(index, 1);
                        console.log(response.data);
                        })
                    }
                },
            },
            mounted() {
                axios.get('api/users').then(response => this.users = response.data.users);
            }
        });
    </script>
</body>

</html>