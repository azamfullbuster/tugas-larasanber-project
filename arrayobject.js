//Soal 1
console.log("Soal 1");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortBuah = daftarBuah.sort();
for (var num = 0; num < sortBuah.length; num++) {
    console.log(sortBuah[num])
}
console.log("\n");

//Soal 2
console.log("Soal 2");
var kalimat = "saya sangat senang belajar javascript";
var kamilat_arr = kalimat.split(' ');
console.log(kamilat_arr);
console.log("\n");

//Soal 3
console.log("Soal 3");
var buah = [{ nama: "strawberry", warna: "merah", "ada bijinya": "tidak", harga: 9000 },
{ nama: "jeruk", warna: "oranye", "ada bijinya": "ada", harga: 8000 },
{ nama: "Semangka", warna: "Hijau & Merah", "ada bijinya": "ada", harga: 10000 },
{ nama: "Pisang", warna: "Kuning", "ada bijinya": "tidak", harga: 10000 }];

buah.forEach(function (item, index) {
    console.log("nama" + (index + 1) + " : " + item.nama)
});
console.log("\n");
console.log("Nama1 :" + buah[0].nama);
console.log("Warna1 :" + buah[0].warna);
console.log("Biji1 :" + buah[0]["ada bijinya"]);
console.log("Harga1 :" + buah[0].harga);

